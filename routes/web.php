<?php

Route::namespace('manager')->group(function(){

    Route::get('/dashboardManager', 'homeController@index')->name('manager.home');

    Route::group(['prefix' => 'barang', 'as' =>'barangManager.'], function(){
        Route::get('/pengadaanBarang', 'barangController@index')->name('index');
    });

    Route::group(['prefix' => 'pembangunan', 'as' =>'pembangunanManager.'], function(){
        Route::get('/kebutuhanPembangunan', 'pembangunanController@index')->name('index');
    });
});

Route::namespace('perencanaan')->group(function(){

    Route::get('/dashboardPerencanaan', 'homeController@index')->name('perencanaan.home');

    Route::group(['prefix' => 'barangPerencanaan', 'as' =>'barangPerencanaan.'], function(){
        Route::get('/index', 'barangController@index')->name('index');
        Route::get('/create', 'barangController@create')->name('create');
    });

    Route::group(['prefix' => 'pembangunanPerencanaan', 'as' =>'pembangunanPerencanaan.'], function(){
        Route::get('/index', 'pembangunanController@index')->name('index');
        Route::get('/create', 'pembangunanController@create')->name('create');
    });

    Route::group(['prefix' => 'stockPerencanaan', 'as' =>'stockPerencanaan.'], function(){
        Route::get('/index', 'stockBarangController@index')->name('index');
    });
});

Route::namespace('kontruksi')->group(function(){

    Route::get('/dashboardKontruksi', 'homeController@index')->name('kontruksi.home');

    Route::group(['prefix' => 'barangKontruksi', 'as' =>'barangKontruksi.'], function(){
        Route::get('/index', 'barangController@index')->name('index');
        Route::get('/create', 'barangController@create')->name('create');
    });

    Route::group(['prefix' => 'pembangunanKontruksi', 'as' =>'pembangunanKontruksi.'], function(){
        Route::get('/index', 'pembangunanController@index')->name('index');
    });

    Route::group(['prefix' => 'stockKontruksi', 'as' =>'stockKontruksi.'], function(){
        Route::get('/index', 'stockBarangController@index')->name('index');
        Route::get('/create', 'stockBarangController@create')->name('create');
    });
});

