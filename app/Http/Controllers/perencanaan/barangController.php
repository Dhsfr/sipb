<?php

namespace App\Http\Controllers\perencanaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class barangController extends Controller{

    public function index(){
        return view('perencanaan.barang.index');
    }

    public function create(){
        return view('perencanaan.barang.create');
    }

    public function store(Request $request){
        
    }

    public function show($id){
        
    }

    public function edit($id){
        
    }

    public function update(Request $request, $id){
        
    }

    public function destroy($id){
        
    }
}
