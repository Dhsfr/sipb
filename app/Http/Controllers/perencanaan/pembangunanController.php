<?php

namespace App\Http\Controllers\perencanaan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class pembangunanController extends Controller{

    public function index(){
        return view('perencanaan.pembangunan.index');
    }

    public function create(){
        return view('perencanaan.pembangunan.create');
    }

    public function store(Request $request){
        
    }

    public function show($id){
        
    }
    
    public function edit($id){
        
    }

    public function update(Request $request, $id){
        
    }

    public function destroy($id){
        
    }
}
